package com.example.BlockBuster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BlockBuster.dto.RolDto;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;
import com.example.BlockBuster.service.impl.RolServiceImpl;

@RestController
@RequestMapping("/api") //TODO: Prueba
@CrossOrigin(origins = "http://localhost:4200", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class RolController {
	
	@Autowired
	private RolServiceImpl rolService;
	
	

	//POST
//	@PostMapping("/addRol")  // Alomejor da problemas al meter ambos como parametro
//	public ResponseEntity<String> addRol(@RequestHeader String rol,@RequestParam String documentoCliente) throws Exception{
//	return new ResponseEntity<String>(rolService.addRol(rol, documentoCliente), HttpStatus.OK);
//			
//	}

	
	//POST
	@PostMapping("/addAdmin") 
	public ResponseEntity<String> addAdmin(@RequestParam String documentoCliente) throws Exception{
	return new ResponseEntity<String>(rolService.addAdmin(documentoCliente), HttpStatus.OK);
			
	}
	
	//POST
	@PostMapping("/initRol") 
	public void initRol(@RequestParam Boolean añadirRoles) {
	rolService.initRol(añadirRoles);
	}
}
