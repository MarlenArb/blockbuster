package com.example.BlockBuster.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BlockBuster.dto.CompanyDto;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.service.impl.CompanyServiceImpl;

@RestController
@RequestMapping("/api") 
@CrossOrigin(origins = "http://localhost:4200", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class CompanyController {
	
	@Autowired
	private CompanyServiceImpl companyService;
	
	//GET
	@GetMapping("/company/{nombreCompany}")
	public ResponseEntity<CompanyDto> getCompany(@PathVariable String nombreCompany ) {	
		return new ResponseEntity<CompanyDto>(companyService.getCompany(nombreCompany), HttpStatus.OK);
	}
	
	//POST
	@PostMapping("/company")  
	public ResponseEntity<CompanyDto> addCompany(@RequestBody CompanyDto company) {
			return new ResponseEntity<CompanyDto>(companyService.addCompany(company), HttpStatus.OK);
			
	};
	
	//DELETE
	@DeleteMapping("/company/{nombreCompany}") 
	public void removeCompany(@PathVariable String nombreCompany) {
		companyService.removeCompany(nombreCompany);
	}
	
	//GET
	@GetMapping("/company")
	public List<CompanyDto> getLista() {
		return companyService.getLista();
	}
	
	//PUT
	@PutMapping("/company/{nombreCompany}")  
	public ResponseEntity<CompanyDto> modify(@PathVariable String nombreCompany,@Validated  @RequestBody CompanyDto companyMod) {
		return new ResponseEntity<CompanyDto>(companyService.modify(nombreCompany, companyMod), HttpStatus.OK);
	}
	
}
