package com.example.BlockBuster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BlockBuster.dto.JuegoDto;
import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.service.impl.JuegoServiceImpl;

@RestController
@RequestMapping("/api") //TODO: Prueba
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class JuegoController {
	
	@Autowired
	private JuegoServiceImpl juegoService;
	
	
		//GET
		@GetMapping("/juego/{refJuego}")
		public ResponseEntity<JuegoDto> getJuego(@PathVariable String refJuego ) throws Exception {	
			return new ResponseEntity<JuegoDto>(juegoService.getJuego(refJuego), HttpStatus.OK);
		}
		

	
	    //POST
		@PostMapping("/juego")  
		public ResponseEntity<JuegoDto> addJuego(@RequestBody JuegoDto juegoDto) throws Exception {
				return new ResponseEntity<JuegoDto>( juegoService.addJuego(juegoDto), HttpStatus.OK);
				
		}
		
		//DELETE
		@DeleteMapping("/juego/{refJuego}") 
		public void removeJuego(@PathVariable String refJuego) throws Exception {
			juegoService.removeJuego(refJuego);
		}
		
		//GET
		@GetMapping("/juego")
		public List<JuegoDto> getLista() throws Exception {
			return juegoService.getLista();
		}
		
		//PUT
		@PutMapping("/juego/{refJuego}")  
		public ResponseEntity<JuegoDto>  modify(@PathVariable String refJuego, @RequestBody JuegoDto juegoMod) throws Exception {
			return new ResponseEntity<JuegoDto>( juegoService.modify(refJuego, juegoMod), HttpStatus.OK);
		}

		
}
