package com.example.BlockBuster.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping("/api") //TODO: Prueba
@CrossOrigin(origins = "http://localhost:4200", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class ClienteController {
	
	@Autowired
	private ClienteServiceImpl clienteService;
	
	//GET
	@GetMapping("/cliente/{documento}")
	public ResponseEntity<ClienteDTO> getCliente(@Validated @PathVariable String documento ) throws Exception{	
		return new ResponseEntity<ClienteDTO>(clienteService.getCliente(documento), HttpStatus.OK);
	}
	
	//POST
	@PostMapping("/cliente")  
	public ResponseEntity<ClienteDTO> addCliente(@Validated @RequestBody ClienteDTO cliente) throws Exception {
			return new ResponseEntity<ClienteDTO>(clienteService.addCliente(cliente), HttpStatus.OK);
			
	};
	
	//DELETE
	@DeleteMapping("/cliente/{documento}") 
	public void removeCliente(@PathVariable String documento) throws Exception {
		clienteService.removeCliente(documento);
	}
	
	//GET
	@GetMapping("/cliente")
	public List<ClienteDTO> getLista() throws Exception {
		return clienteService.getLista();
	}
	
	//PUT
	@PutMapping("/cliente/{documento}")  
	public ResponseEntity<ClienteDTO> modify(@Validated @PathVariable String documento,@Validated  @RequestBody ClienteDTO clienteMod) throws Exception{
		return new ResponseEntity<ClienteDTO>(clienteService.modify(documento, clienteMod), HttpStatus.OK);
	}
	
	//POSTCONSTRUCTOR
	@PostConstruct
	public void agregarJson() {
		clienteService.agregarJson();
	}
	
}
	

