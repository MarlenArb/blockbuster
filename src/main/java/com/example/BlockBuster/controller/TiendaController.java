package com.example.BlockBuster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BlockBuster.dto.TiendaDto;
import com.example.BlockBuster.entities.Tienda;
import com.example.BlockBuster.service.impl.TiendaServiceImpl;

@RestController
@RequestMapping("/api") //TODO: Prueba
@CrossOrigin(origins = "http://localhost:4200", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class TiendaController {
	
	@Autowired
	private TiendaServiceImpl tiendaService;
	
	
		//GET
		@GetMapping("/tienda/{nombreTienda}")
		public ResponseEntity<TiendaDto> getTienda(@Validated @PathVariable String nombreTienda ) throws Exception {	
			return new ResponseEntity<TiendaDto>(tiendaService.getTienda(nombreTienda),HttpStatus.OK);
		}
		

		//POST
		@PostMapping("/tienda")  
		public ResponseEntity<TiendaDto> addTienda(@Validated @RequestBody TiendaDto tiendaDto) throws Exception {
				return new ResponseEntity<TiendaDto>(tiendaService.addTienda(tiendaDto), HttpStatus.OK);
				
		};
		
		//DELETE
		@DeleteMapping("/tienda/{nombreTienda}") 
		public void removeTienda(@PathVariable String nombreTienda) throws Exception {
			tiendaService.removeTienda(nombreTienda);
		}
		
		//GET
		@GetMapping("/tienda")
		public List<TiendaDto> getLista() throws Exception {
			return tiendaService.getLista();
		}
		
		//PUT
		@PutMapping("/tienda/{nombreTienda}")  
		public ResponseEntity<TiendaDto> modify(@Validated @PathVariable String nombreTienda, @Validated @RequestBody TiendaDto tiendaMod) throws Exception {
			return new ResponseEntity<TiendaDto>(tiendaService.modify(nombreTienda, tiendaMod), HttpStatus.OK);
		}


}
