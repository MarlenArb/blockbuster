package com.example.BlockBuster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.service.impl.PedidoServiceImpl;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class PedidoController {
	
	@Autowired
	private PedidoServiceImpl pedidoService;
	
	
	//GET
	@GetMapping("/pedido/{refPedido}")
	public ResponseEntity<PedidoDto> getPedido(@PathVariable String refPedido ) throws Exception {	
		return new ResponseEntity<PedidoDto>(pedidoService.getPedido(refPedido),HttpStatus.OK);
	}
	

	//POST
	@PostMapping("/pedido")  
	public ResponseEntity<PedidoDto> addPedido(@RequestBody PedidoDto pedidoDto) throws Exception {
			return new ResponseEntity<PedidoDto>( pedidoService.addPedido(pedidoDto), HttpStatus.OK);
			
	};
	
	//DELETE
	@DeleteMapping("/pedido/{refPedido}") 
	public void removePedido(@PathVariable String refPedido) throws Exception {
		pedidoService.removePedido(refPedido);
	}
	
	//GET
	@GetMapping("/pedido")
	public List<PedidoDto> getLista() throws Exception {
		return pedidoService.getLista();
	}
	
	//PUT
	@PutMapping("/pedido/{refPedido}")  
	public ResponseEntity<PedidoDto> modify(@PathVariable String refPedido,@Validated  @RequestBody PedidoDto pedidoMod) throws Exception{
		return new ResponseEntity<PedidoDto>(pedidoService.modify(refPedido, pedidoMod), HttpStatus.OK);
	}

}
