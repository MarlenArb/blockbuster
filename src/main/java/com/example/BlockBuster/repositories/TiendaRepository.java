package com.example.BlockBuster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.example.BlockBuster.entities.Tienda;

@Repository
public interface TiendaRepository extends JpaRepository<Tienda, Long> {

	public Tienda findByNombreTienda(@Param("nombreTienda") String nombreTienda);
}
