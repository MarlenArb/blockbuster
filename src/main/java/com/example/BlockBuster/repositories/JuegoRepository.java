package com.example.BlockBuster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.entities.Pedido;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Long> {

	public Juego findByRefJuego(@Param("refJuego") String refJuego);
}

