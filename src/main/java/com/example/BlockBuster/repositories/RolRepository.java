package com.example.BlockBuster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;

@Repository 
public interface RolRepository extends JpaRepository<Rol, Long>{

	public Rol findByRol(@Param("rol") String rol); 
}
