package com.example.BlockBuster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BlockBuster.entities.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	public Cliente findByDocumento(@Param("documento") String documento);
	public Cliente findByUsername(@Param("username") String username);
}
