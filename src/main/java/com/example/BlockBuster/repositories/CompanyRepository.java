package com.example.BlockBuster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Juego;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{
	public Company findByNombreCompany(@Param("nombreCompany") String nombreCompany);
}