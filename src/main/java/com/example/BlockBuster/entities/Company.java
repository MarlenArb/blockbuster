package com.example.BlockBuster.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "COMPANY")
@Data
public class Company {
	
	@Id 
	@Column(name = "ID")
	@GeneratedValue( strategy = GenerationType.AUTO) 
	private Long id;
	
	@Column(name = "CIF")
	private String cif;
	
	@Column(name = "NOMBRE_COM")
	private String nombreCompany;
	
	//ESTABLECER RELACIONES AQUI
	// N compañias M juegos
	@ManyToMany(cascade = CascadeType.ALL) 
	private List<Juego> juegos = new ArrayList<Juego>();
	

}
