package com.example.BlockBuster.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.BlockBuster.enums.EnumCategoria;

import lombok.Data;

@Entity
@Table(name = "JUEGO")
@Data
public class Juego implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "TITULO")
	private String titulo;
	
	@Column(name = "FECHA_LANZ")
	private LocalDate fechaLanzamiento;
	
	@Column(name = "PEGI")
	private Integer pegi;
	
	@Column(name = "REF_JUEGO")
	private String refJuego;
	
	
	@Column(name = "CATEGORIA")
	private EnumCategoria categoria;
	
	
	
	//N juegos 1 pedido  --PARA RELACIONAR CON PEDIDOS
	@ManyToOne
	private Pedido pedido = new Pedido();
	
	//N juegos M compañias --PARA RELACIONAR CON COMPAÑIAS
	@ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "juegos") //Apunta a la lista juegos de Compañias entidad
	private List<Company> companies = new ArrayList<Company>();
	
}
