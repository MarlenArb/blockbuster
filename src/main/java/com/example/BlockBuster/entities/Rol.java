package com.example.BlockBuster.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.example.BlockBuster.enums.EnumRol;

import lombok.Data;

@Entity
@Table( name = "ROL")
@Data
public class Rol {
	
	
	@Id //Para decirle que es una PK
	@Column(name = "ID")
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "USER_ROL")
	private String rol;

	//N roles M clientes 
	@ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "roles") //SI DA ERROR PONER .ALL
	private List<Cliente> clientes = new ArrayList<Cliente>();
}
