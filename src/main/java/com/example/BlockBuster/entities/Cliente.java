package com.example.BlockBuster.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.entities.Pedido;//prueba lo añadi manualmente los dos de la misma carpeta no se improta?
import lombok.Data;


@Entity
@Table(name = "CLIENTE")
@Data
public class Cliente implements Serializable{

	//TODO: Probando
	private static final long serialVersionUID = 1L;

	@Id //Para decirle que es una PK
	@Column(name = "ID")
	@GeneratedValue( strategy = GenerationType.AUTO) //Para decirle que se autoincremente
	private Long id;
	
	@Column(name = "NOMBRE")
	private String nombreCliente;
	
	@Column(name = "USERNAME") //si la llamo USER me da un error de SQL
	private String username;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "CORREO")
	private String correo;
	
	@Column(name = "DOCUMENTO")
	private String documento;
	
	@Column(name = "FECHA_NAC")
	//@Temporal(TemporalType.DATE) //TODO: Prueba fecha
	private LocalDate fechaNac;
	
	//1 cliente N pedidos
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente", orphanRemoval = true) //TODO: prueba
	private List<Pedido> pedidos = new ArrayList<Pedido>();
	
	
	
	//N clientes M roles
	@ManyToMany(cascade = CascadeType.PERSIST)//, mappedBy = "cliente") //SI DA ERROR PONER .ALL
	private List<Rol> roles = new ArrayList<Rol>();

}
