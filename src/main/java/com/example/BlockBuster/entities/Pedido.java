package com.example.BlockBuster.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "PEDIDO")
@Data
public class Pedido {
	
	@Id //Para decirle que es una PK
	@Column(name = "ID")
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	
	
	@Column(name = "ESTADO")
	private String estado;
	
	@Column(name = "REF_PEDIDO")
	private String refPedido;
	
	// N pedidos 1 Cliente
	@ManyToOne
	private Cliente cliente = new Cliente();
	
	// 1 pedido N juegos
	@OneToMany(mappedBy = "pedido") 
	private List<Juego> juegos = new ArrayList<Juego>();
	
	// N pedidos 1 tienda
	@ManyToOne
	private Tienda tienda = new Tienda();
	
}
