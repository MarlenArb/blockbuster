package com.example.BlockBuster.dto;

import java.util.List;

import lombok.Data;

@Data
public class ErrorResponse {
	
	private String exception;
	private String message;
	private String path;
	private List<String> valids;
	
	public ErrorResponse(Exception exception, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = exception.getMessage();
		this.path = path;
	}
	
	public ErrorResponse(Exception exception,List<String> message, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.valids = message;
		this.path = path;
	}
}

