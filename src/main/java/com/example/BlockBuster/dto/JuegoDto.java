package com.example.BlockBuster.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.enums.EnumCategoria;

import lombok.Data;

@Data
public class JuegoDto {
	private String titulo;
	private LocalDate fechaLanzamiento;
	private Integer pegi;
	private String refJuego;
	private EnumCategoria categoria;
	private String refPedido;
	private List<String> companies = new ArrayList<>();

}
