package com.example.BlockBuster.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TiendaDto {
	
	@NotBlank(message = "El campo nombre no puede estar en blanco")
	@NotNull(message = "El campo nombre no puede estar vacío")
	private String nombreTienda;
	
	@NotBlank(message = "El campo direccion no puede estar en blanco")
	@NotNull(message = "El campo direccion no puede estar vacío")
	private String direccion;
	
	
}
