package com.example.BlockBuster.dto;

import java.util.ArrayList;
import java.util.List;

import com.example.BlockBuster.entities.Juego;

import lombok.Data;

@Data
public class PedidoDto {
	
	private String estado;
	private String refPedido;
	private String documentoCliente; //client who makes the order
	private String nombreTienda; //the store where I make the order
	
	
}
