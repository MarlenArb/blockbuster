package com.example.BlockBuster.dto;

import com.example.BlockBuster.enums.EnumRol;

import lombok.Data;

@Data
public class RolDto {

	private EnumRol rol;
}
