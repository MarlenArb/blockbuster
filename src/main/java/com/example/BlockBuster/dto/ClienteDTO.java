package com.example.BlockBuster.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ClienteDTO {

	
	@NotNull(message = "El campo nombre no puede estar vacío")
	//@NotBlank(message = "El campo nombre no puede estar en blanco")
	@Size(min = 3, max = 20, message = "El nombre debe estar contenido en un rango de 3-50 caracteres")
	private String nombreCliente;
	
	@Size(min = 3, message = "La constraseña debe tener mínimo 3 caracteres")
	@NotNull(message = "El campo password no puede estar vacío")
	private String password;
	
	@NotNull(message = "El campo correo no puede estar vacío")
	//@NotBlank(message = "El campo correo no puede estar en blanco")
	@Email(message = "El campo correo tiene un formato no válido")
	private String correo;
	
	

	@NotNull(message = "El campo documento no puede estar vacío")
	@Pattern(regexp = "[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]", message = "El documento no tiene un formato de DNI válido")
	private String documento;
	
	

	@NotNull(message = "El campo fechaNacimiento no puede estar vacío")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
	private LocalDate fechaNac;
	private List<EnumRol> roles = new ArrayList<>();

}
	
