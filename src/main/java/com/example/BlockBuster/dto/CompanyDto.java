package com.example.BlockBuster.dto;

import java.util.ArrayList;
import java.util.List;

import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.enums.EnumRol;

import lombok.Data;

@Data
public class CompanyDto {

	private String cif;
	private String nombreCompany;
	private List<String> juegos = new ArrayList<>();
}
