package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.JuegoDto;
import com.example.BlockBuster.entities.Juego;


@Service
public interface JuegoService {
	
	public JuegoDto getJuego(String refJuego ) throws Exception;	 
	public JuegoDto addJuego(JuegoDto juegoDto) throws Exception;
	public void removeJuego(String refJuego) throws Exception;
	public List<JuegoDto> getLista() throws Exception;
	public JuegoDto modify(String refJuego, JuegoDto juegoMod) throws Exception;
	
}
