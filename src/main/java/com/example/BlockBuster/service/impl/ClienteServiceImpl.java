package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteBadRequestException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteForbiddenException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteImUsedException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteNoContentException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteNotFoundException;
import com.example.BlockBuster.exceptions.generic.BadRequestException;
import com.example.BlockBuster.exceptions.generic.NotFoundException;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.PedidoRepository;
import com.example.BlockBuster.service.ClienteService;
import com.example.BlockBuster.service.Converters.ClienteConverter;
import com.google.gson.Gson;


@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private ClienteService clienteService; //no la uso?
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ClienteConverter clienteConverter;
	
	private List<Cliente> clientes = new ArrayList<>();
	private List<ClienteDTO> clientesDto = new ArrayList<>();
	private List<Pedido> pedidos = new ArrayList<>();
	
	//TODO: Hacer todos los loggers como en cliente
	Logger logger = LoggerFactory.getLogger(ClienteServiceImpl.class);
	
	//Look for a client in data base. --OK
	@Override
	public ClienteDTO getCliente(String documento) throws Exception { //DEVUELVE EL OBJETO PORQUE LA ENTIDAD DA BUCLE AL TENER LISTAPEDIDOS
		Cliente c = new Cliente();
		ClienteDTO cdto = new ClienteDTO();
		 c = clienteRepository.findByDocumento(documento);
		 if (c != null) {
			 cdto = clienteConverter.convertClienteToDto(c);
			 return cdto;
		 }
		 else {
			 logger.error("No existe ningún cliente con el documento: " + documento);
			 throw new ClienteNotFoundException("No existe ningún cliente con el documento: " + documento);
		 }
	}
	
	
	//Add new client in a data base --OK
	@Override
	public ClienteDTO addCliente(ClienteDTO clienteDto) throws Exception{
		Cliente newCliente = new Cliente();
		newCliente = clienteRepository.findByDocumento(clienteDto.getDocumento());
		if (newCliente == null){
			newCliente = clienteConverter.convertClienteToEntity(clienteDto);
			clienteRepository.save(newCliente);
			ClienteDTO clienteDtoMod = new ClienteDTO();
			clienteDtoMod = clienteConverter.convertClienteToDto(newCliente);
			return clienteDtoMod;
		}
		else { 
			logger.error("Ya existe un cliente con el documento: " + clienteDto.getDocumento());
			throw new ClienteForbiddenException("Ya existe un cliente con el documento: " + clienteDto.getDocumento()); // OK
		}
	}


	//TODO: No me borra los clientes con pedidos
	
	//Remove an exists client from a data base --OK
	@Override 
	public void removeCliente(String documento) throws Exception{
		Cliente cliente = new Cliente();
		cliente = clienteRepository.findByDocumento(documento);
		if( cliente != null) {
			
//			for(Pedido pedido : cliente.getPedidos()) {
//				//Pedido pedidoCliente = pedidoRepository.findByRefPedido(pedido.getRefPedido());
//				pedidoRepository.delete( pedidoRepository.findByRefPedido(pedido.getRefPedido()));
//			}
			//TODO: No borra los clientes con pedidos asociados
			
			
			
			clienteRepository.delete(cliente);
				//return ("Cliente borrado de la base de datos");
			}
			else {
				logger.error("No existe ningún cliente con el documento: " + documento);
				throw new ClienteBadRequestException("No existe ningún cliente con el documento indicado"); // OK
			}
	}
	

	//Show me all the clients from a data base --OK 
	@Override
	public List<ClienteDTO> getLista() throws Exception {
		clientesDto.clear();
		clientes = clienteRepository.findAll();
		ClienteDTO clienteDto = new ClienteDTO();
		for(Cliente cliente: clientes) {
			clienteDto = clienteConverter.convertClienteToDto(cliente);
			clientesDto.add(clienteDto);
		}
		if( clientes.size() != 0) {
			return clientesDto;
			
		}
		
		else {
			logger.error("No hay ningún cliente registrado en la base de datos");
			throw new ClienteBadRequestException("No hay ningún cliente registrado en la base de datos"); 
			//TODO: funciona pero deberia ser nocontent, lo que ese no me muestra mensaje ninguno  
		}
	}
	
	
	//Modify a client from a data base --OK
	@Transactional
	@Override
	public ClienteDTO modify(String documento, ClienteDTO clienteMod) throws Exception {
		Cliente c = new Cliente();
		c = clienteRepository.findByDocumento(documento); 
		
		if( c != null) {
			

			//c = clienteConverter.convertClienteToEntity(clienteMod);
			if(clienteMod.getNombreCliente() != null) {
				c.setNombreCliente(clienteMod.getNombreCliente());
			}
			
			if(clienteMod.getPassword() != null) {
				c.setPassword(encoder.encode(clienteMod.getPassword()));
			}
			
			if(clienteMod.getCorreo() != null) {
				c.setCorreo(clienteMod.getCorreo());
			}
			
			if(clienteMod.getDocumento() != null) {
				c.setDocumento(clienteMod.getDocumento());
			}
			
			if(clienteMod.getFechaNac()!= null) {
				c.setFechaNac(clienteMod.getFechaNac());
			}
			
			clienteRepository.save(c);
			
			ClienteDTO clienteDtoMod = new ClienteDTO();
			clienteDtoMod = clienteConverter.convertClienteToDto(c);
			return clienteDtoMod;
		}
		
		else {
			logger.error("No existe ningún cliente con el documento: " + documento);
			throw new ClienteBadRequestException("No existe ningún cliente con el documento: " +  documento);
		}
	}


	@Override
	public void agregarJson() {
		// TODO Auto-generated method stub
		
	}


	
	
	
	//Add new clients from .JSON
//	@Override
//	public void agregarJson() {
//		JSONParser parser = new JSONParser();
//		
//		try {
//			Object obj = parser.parse(new FileReader("clientes.json"));
//			JSONObject jsonObject = (JSONObject) obj;
//			
//			Cliente nuevoCliente = new Cliente();
//			nuevoCliente.setNombreCliente((String) jsonObject.get("nombreCliente"));
//			nuevoCliente.setCorreo((String) jsonObject.get("correo"));
//			nuevoCliente.setDocumento((String) jsonObject.get("documento"));
//			String fechaNac = (String) jsonObject.get("fechaNac").toString();
//			nuevoCliente.setFechaNac(LocalDate.parse(fechaNac, DateTimeFormatter.ofPattern("dd/MM/yyyy"))); 
//			
//			System.out.println(nuevoCliente);		
//			clientes.add(nuevoCliente);
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//	} 
	//funciona pero solo con un cliente
	
//	@Override
//	public void agregarJson() {
//		JSONParser parser = new JSONParser();
//		
//		try {
//			Object obj = parser.parse(new FileReader("clientes.json"));
//			JSONObject jsonObject = (JSONObject) obj;
//			
//			while((String) jsonObject.get("nombreCliente") != null) { //mientras haya un nombre que siga grabando??
//			Cliente nuevoCliente = new Cliente();
//			nuevoCliente.setNombreCliente((String) jsonObject.get("nombreCliente"));
//			nuevoCliente.setCorreo((String) jsonObject.get("correo"));
//			nuevoCliente.setDocumento((String) jsonObject.get("documento"));
//			String fechaNac = (String) jsonObject.get("fechaNac").toString();
//			nuevoCliente.setFechaNac(LocalDate.parse(fechaNac, DateTimeFormatter.ofPattern("dd/MM/yyyy"))); 
//			
//			clientes.add(nuevoCliente);
//			System.out.println(nuevoCliente);
//			}
//					
//			
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//	}
	
	
	
	
}
