package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.JuegoDto;
import com.example.BlockBuster.dto.RolDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.RolRepository;
import com.example.BlockBuster.service.RolService;
import com.example.BlockBuster.service.Converters.PedidoConverter;

@Service
public class RolServiceImpl implements RolService{
	
	
	Logger logger = LoggerFactory.getLogger(RolServiceImpl.class);
	
	@Autowired
	private RolRepository rolRepository;
	
	@Autowired
	private ClienteRepository  clienteRepository;
	
	private List<Rol> rolesDto;
	private List<Cliente> clientes;
	
	@Override
	public void initRol(Boolean añadirRoles) {
		if(añadirRoles) {
			Rol rolAdmin = new Rol();
			Rol rolUser = new Rol();
			rolAdmin.setRol("ADMIN");
			rolUser.setRol("USER");
			rolRepository.save(rolAdmin);
			rolRepository.save(rolUser);
		}
	}
	
	@Override
	public String addAdmin(String documentoCliente) throws Exception {
		
		Cliente c = clienteRepository.findByDocumento(documentoCliente);
		Rol rolAdmin = new Rol();
		rolAdmin = rolRepository.findByRol("ADMIN");
		c.getRoles().add(rolAdmin);
		rolAdmin.getClientes().add(c);
		rolRepository.save(rolAdmin);
		
		return "Ahora el cliente con documento: " + documentoCliente + " tiene privilegios de: ADMIN" ;
	}
}
