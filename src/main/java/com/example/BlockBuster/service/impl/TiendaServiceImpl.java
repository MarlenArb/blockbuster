package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.dto.TiendaDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Tienda;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteBadRequestException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteNotFoundException;
import com.example.BlockBuster.exceptions.tiendaExceptions.TiendaBadRequestException;
import com.example.BlockBuster.exceptions.tiendaExceptions.TiendaForbiddenException;
import com.example.BlockBuster.exceptions.tiendaExceptions.TiendaImUsedException;
import com.example.BlockBuster.exceptions.tiendaExceptions.TiendaNotFoundException;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.TiendaRepository;
import com.example.BlockBuster.service.ClienteService;
import com.example.BlockBuster.service.PedidoService;
import com.example.BlockBuster.service.TiendaService;
import com.example.BlockBuster.service.Converters.TiendaConverter;

@Service
public class TiendaServiceImpl implements TiendaService{
	
		Logger logger = LoggerFactory.getLogger(TiendaServiceImpl.class);
	
		@Autowired
		private TiendaService tiendaService; //no la uso?
		
		@Autowired
		private TiendaRepository tiendaRepository;
		
		@Autowired
		private TiendaConverter tiendaConverter;
		
		@Autowired 
		private PedidoService pedidoService;

		private List<Tienda> tiendas = new ArrayList<>();
		private List<TiendaDto> tiendasDto = new ArrayList<>();
		
	//Look for a store in a data base --OK	
	@Override
	public TiendaDto getTienda(String nombreTienda) throws Exception {
		
		Tienda t = new Tienda();
		TiendaDto tdto = new TiendaDto();
		 t = tiendaRepository.findByNombreTienda(nombreTienda);
		 if (t != null) {
			 tdto = tiendaConverter.convertTiendaToDto(t);
			 return tdto;
		 }
		 else 
			 throw new TiendaNotFoundException("No existe ninguna tienda con el nombre introducido");
	}

	//Add new store in a data base --OK
	@Override
	public TiendaDto addTienda(TiendaDto tiendaDto) throws Exception  {
		Tienda newTienda = new Tienda();
		newTienda = tiendaRepository.findByNombreTienda(tiendaDto.getNombreTienda());
		if (newTienda == null){
			
			newTienda = tiendaConverter.convertTiendaToEntity(tiendaDto);
			tiendaRepository.save(newTienda);
			TiendaDto tiendaDtoMod = new TiendaDto();
			tiendaDtoMod = tiendaConverter.convertTiendaToDto(newTienda);
			return tiendaDtoMod;
		}
		else 
			throw new TiendaForbiddenException("Ya existe una tienda con el nombre: " + tiendaDto.getNombreTienda()); // OK
	}
	

	//TODO: NO ME BORRA LOS PEDIDOS ASOCIADOS, ERROR: java.sql.SQLIntegrityConstraintViolationException
	@Override
	public void removeTienda(String nombreTienda) throws Exception  {
		Tienda t = new Tienda();
		t = tiendaRepository.findByNombreTienda(nombreTienda);
		if( t != null) {
			tiendaRepository.delete(t);
		}
		else
			throw new TiendaBadRequestException("No existe ninguna tienda con el nombre introducido"); // OK
	}

	
	@Override
	public List<TiendaDto> getLista() throws Exception  {
		tiendasDto.clear();
		tiendas = tiendaRepository.findAll();
		TiendaDto tiendaDto = new TiendaDto();
		for(Tienda tienda: tiendas) {
			tiendaDto = tiendaConverter.convertTiendaToDto(tienda);
			tiendasDto.add(tiendaDto);
		}
		if( tiendas.size() != 0) {
			return tiendasDto;
			
		}
		
		else
			throw new TiendaBadRequestException("No hay ninguna tienda registrada en la base de datos"); 
			//TODO: funciona pero deberia ser nocontent, lo que ese no me muestra mensaje ninguno  
		
	}

	@Override
	public TiendaDto modify(String nombreTienda, TiendaDto tiendaMod) throws Exception  { //mirar si lo escribi bien en el service
		Tienda t = new Tienda();
		t = tiendaRepository.findByNombreTienda(nombreTienda); 
		
		if(t != null) {
			
			if(tiendaMod.getNombreTienda() != null) {
				t.setNombreTienda(tiendaMod.getNombreTienda());;
			}
			
			if(tiendaMod.getDireccion() != null) {
				t.setDireccion(tiendaMod.getDireccion());;
			}
			
			
			tiendaRepository.save(t);
			
			TiendaDto tiendaDtoMod = new TiendaDto();
			tiendaDtoMod = tiendaConverter.convertTiendaToDto(t);
			return tiendaDtoMod;
		}
		else
			throw new TiendaBadRequestException("No existe ninguna tienda con el nombre: " + tiendaMod.getNombreTienda());
	}
}
