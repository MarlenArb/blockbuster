package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.dto.JuegoDto;
import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.entities.Tienda;
import com.example.BlockBuster.enums.EnumCategoria;
import com.example.BlockBuster.exceptions.generic.BadRequestException;
import com.example.BlockBuster.exceptions.juegoExceptions.JuegoBadRequestException;
import com.example.BlockBuster.exceptions.juegoExceptions.JuegoForbiddenException;
import com.example.BlockBuster.exceptions.juegoExceptions.JuegoImUsedException;
import com.example.BlockBuster.exceptions.juegoExceptions.JuegoNoContentException;
import com.example.BlockBuster.exceptions.juegoExceptions.JuegoNotFoundException;
import com.example.BlockBuster.exceptions.pedidoExceptions.PedidoBadRequestException;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.CompanyRepository;
import com.example.BlockBuster.repositories.JuegoRepository;
import com.example.BlockBuster.repositories.PedidoRepository;
import com.example.BlockBuster.repositories.TiendaRepository;
import com.example.BlockBuster.service.JuegoService;
import com.example.BlockBuster.service.Converters.JuegoConverter;


@Service
public class JuegoServiceImpl implements JuegoService {
	
	Logger logger = LoggerFactory.getLogger(JuegoServiceImpl.class);

	@Autowired
	private JuegoRepository  juegoRepository; 
	
	@Autowired
	private JuegoConverter  juegoConverter; 
	
	@Autowired
	private PedidoRepository pedidoRepository; // N juegos 1 pedido
	
	@Autowired
	private TiendaRepository tiendaRepository; //N juegos 1 tienda
	
	
	@Autowired
	private CompanyRepository companyRepository; //N juegos M companies
	
	private List<Juego> juegos = new ArrayList<>();
	private List<JuegoDto> juegosDto = new ArrayList<>();
	private List<Company> companies = new ArrayList<>();
	private List<String> nombreCompanies = new ArrayList<>();

	//Add new game to data base --OK
	@Override
	public JuegoDto addJuego(JuegoDto juegoDto) throws Exception {
	
			Juego newJuego = new Juego(); 
			newJuego = juegoRepository.findByRefJuego(juegoDto.getRefJuego());
			
			if(newJuego == null) {
				Pedido p = pedidoRepository.findByRefPedido(juegoDto.getRefPedido());
				if(p == null) {
					throw new JuegoNotFoundException("No existe ningún pedido con la referencia introducida");
				}
				newJuego = juegoConverter.convertJuegoToEntity(juegoDto); 
				
				//Añado el newJuego a la lista de juegos del pedido al que pertenece ( N juegos 1 pedido )
				//El añadir la refPedido al juego está hecho en el converter
				p.getJuegos().add(newJuego); 
				
				//Añado el newJuego a las diferentes compañias y viceversa
				nombreCompanies = juegoDto.getCompanies();
				for(String nombreCompany : nombreCompanies) {
					Company c = companyRepository.findByNombreCompany(nombreCompany); //TODO: Prueba, lo pase a string
					c.getJuegos().add(newJuego); // N juegos M companies
					newJuego.getCompanies().add(c);
					//companyRepository.save(c);
					
					
				}
				juegoRepository.save(newJuego);
				
				JuegoDto juegoDtoMod = new JuegoDto();
				juegoDtoMod = juegoConverter.convertJuegoToDto(newJuego);
				return juegoDtoMod;
			}
			else {
				logger.error("Ya hay un juego en la base de datos con la referencia: " + juegoDto.getRefJuego());
				throw new JuegoForbiddenException("Ya hay un juego en la base de datos con la referencia: " + juegoDto.getRefJuego());
			}
	}
	
	
	
	//Remove an exists game  --PROBAR
	@Override
	public void removeJuego(String refJuego) throws Exception {
		
		Juego j = new Juego();
		j = juegoRepository.findByRefJuego(refJuego);
		if( j != null) {
		juegoRepository.delete(j);
			System.out.println("Juego borrado de la base de datos");
		}
		else {
			
			logger.error("No existe ningun juego con la referencia: " + refJuego);
			throw new JuegoNotFoundException("No existe ningun juego con la referencia: " + refJuego);
		}
	}
	

	//Show me all the games --
	@Override
	public List<JuegoDto> getLista() throws Exception {
		juegosDto.clear();
		juegos = juegoRepository.findAll();
		if( juegos.size() != 0) {
			JuegoDto juegoDto = new JuegoDto();
			for(Juego juego: juegos) {
				juegoDto = juegoConverter.convertJuegoToDto(juego); //TODO: Da bucle
				juegosDto.add(juegoDto);
				}
			
				return juegosDto;
			}
		else {
			logger.error("No hay ningún juego registrado en la base de datos");
			throw new JuegoBadRequestException("No hay ningún juego registrado en la base de datos"); 
			//TODO: funciona pero deberia ser nocontent, lo que ese no me muestra mensaje ninguno  
		}
		
	}

	@Override
	public JuegoDto modify(String refJuego, JuegoDto juegoMod) throws Exception {
		Juego j = new Juego();
		j = juegoRepository.findByRefJuego(refJuego); 
		if (j != null) {
			
			if(juegoMod.getRefJuego()!= null) {
				j.setRefJuego(juegoMod.getRefJuego());;
			}
			
			if(juegoMod.getPegi()!= null) {
				j.setPegi(juegoMod.getPegi());;
			}
			
			if(juegoMod.getCategoria()!= null) {
				j.setCategoria(juegoMod.getCategoria());;
			}
			
			if(juegoMod.getFechaLanzamiento()!= null) {
				j.setFechaLanzamiento(juegoMod.getFechaLanzamiento());
			}
			
			if(juegoMod.getTitulo()!= null) {
				j.setTitulo(juegoMod.getTitulo());
			}
			
			if(juegoMod.getCompanies().size() != 0);{
				//TODO: es una locura porque tendria que borrar el juego de las anteriores compañias tambien
			}
			
			if(juegoMod.getRefPedido() != null) { // --OK
				if(pedidoRepository.findByRefPedido(juegoMod.getRefPedido()) != null) {
				j.setPedido(pedidoRepository.findByRefPedido(juegoMod.getRefPedido()));
				}
				else {
					logger.error("No existe ningún pedido con la referencia: " + juegoMod.getRefPedido());
					throw new JuegoNotFoundException("No existe ningún pedido con la referencia: " + juegoMod.getRefPedido());
				}
			}
		
			juegoRepository.save(j);
			
			JuegoDto juegoDtoMod = new JuegoDto();
			juegoDtoMod = juegoConverter.convertJuegoToDto(j);
			return juegoDtoMod;
			
		}
		else { 
			logger.error("No existe ningún juego con la referencia: " + refJuego);
			throw new JuegoNotFoundException("No existe ningún juego con la referencia: " + refJuego);
		}
	}
		

	//Look for a game from a data base --PROBAR
	@Override
	public JuegoDto getJuego(String refJuego) throws Exception { 
		Juego j = new Juego ();
		j = juegoRepository.findByRefJuego(refJuego);
		if ( j != null) {
			JuegoDto juegoDto = new JuegoDto();
			juegoDto = juegoConverter.convertJuegoToDto(j);
			return juegoDto;
		}
		else {
			logger.error("No existe ningún juego con la referencia: " + refJuego);
			throw new JuegoNotFoundException("No existe ningún juego con la referencia: " + refJuego);
		}
	}



}
