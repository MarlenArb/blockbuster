package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.dto.CompanyDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteBadRequestException;
import com.example.BlockBuster.exceptions.clienteExceptions.ClienteNotFoundException;
import com.example.BlockBuster.exceptions.companyExceptions.CompanyBadRequestException;
import com.example.BlockBuster.exceptions.companyExceptions.CompanyImUsedException;
import com.example.BlockBuster.exceptions.companyExceptions.CompanyNotFoundException;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.CompanyRepository;
import com.example.BlockBuster.service.ClienteService;
import com.example.BlockBuster.service.CompanyService;
import com.example.BlockBuster.service.Converters.ClienteConverter;
import com.example.BlockBuster.service.Converters.CompanyConverter;

@Service
public class CompanyServiceImpl implements CompanyService{
	
	Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);
	
	@Autowired
	private CompanyService companyService; 
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanyConverter companyConverter;
	
	private List<Company> companies = new ArrayList<>();
	private List<CompanyDto> companiesDto = new ArrayList<>();
	
	//Look for a company in data base. --OK
		@Override
		public CompanyDto getCompany(String nombreCompany) {
			Company c = new Company();
			CompanyDto cdto = new CompanyDto();
			 c = companyRepository.findByNombreCompany(nombreCompany);
			 if (c != null) {
				 cdto = companyConverter.convertCompanyToDto(c);
				 return cdto;
			 }
			 else {
				 logger.error("No existe ninguna company con el nombre: " + nombreCompany);
				 throw new CompanyNotFoundException("No existe ninguna company con el nombre: " + nombreCompany); //OK
			 }
		}
		
		
		//Add new company in a data base --OK
		@Override
		public CompanyDto addCompany(CompanyDto companyDto) {
			Company newCompany = new Company();
			newCompany = companyRepository.findByNombreCompany(companyDto.getNombreCompany());
			if(newCompany == null) {
				newCompany = companyConverter.convertCompanyToEntity(companyDto);
				companyRepository.save(newCompany);

				CompanyDto companyDtoMod = new CompanyDto();
				companyDtoMod = companyConverter.convertCompanyToDto(newCompany);
				return companyDtoMod;
			}
			else {
				logger.error("Ya existe una compañia con el nombre: " + companyDto.getNombreCompany());
				throw new CompanyImUsedException("Ya existe una compañia con el nombre: " + companyDto.getNombreCompany()); // OK
			}
		}
			
		


		//Remove an exists company from a data base --OK
		@Override 
		public void removeCompany(String nombreCompany) { 
			Company c = new Company();
			c = companyRepository.findByNombreCompany(nombreCompany);
			if( c != null) {
			companyRepository.delete(c);
				System.out.println("Compañia y sus respectivos juegos borrados de la base de datos");
			}
			else {
				logger.error("No existe ninguna company con el nombre: " + nombreCompany);
				throw new CompanyBadRequestException("No existe ninguna company con el nombre: " + nombreCompany); // OK
			}
		}
		

		//Show me all the companies from a data base --OK
		@Override
		public List<CompanyDto> getLista() {
			companiesDto.clear();
			companies = companyRepository.findAll();
			CompanyDto companyDto = new CompanyDto();
			for(Company company: companies) {
				companyDto = companyConverter.convertCompanyToDto(company);
				companiesDto.add(companyDto);
			}
			if( companies.size() != 0) {
				return companiesDto;
				
			}
			
			else {
				logger.error("No hay ninguna company registrada en la base de datos");
				throw new CompanyBadRequestException("No hay ninguna company registrada en la base de datos"); 
				//TODO: funciona pero deberia ser nocontent, lo que ese no me muestra mensaje ninguno  
			}
		}
		
		
		//Modify a company from a data base --OK
		@Override
		public CompanyDto modify(String nombreCompany, CompanyDto companyMod) {
			Company c = new Company();
			c = companyRepository.findByNombreCompany(nombreCompany); 
			if(c != null) {
				if(companyMod.getNombreCompany() != null) {
					c.setNombreCompany(companyMod.getNombreCompany());;
				}
				
				if(companyMod.getCif() != null) {
					c.setCif(companyMod.getCif());;
				}
	
				companyRepository.save(c);
				
				CompanyDto companyDtoMod = new CompanyDto();
				companyDtoMod = companyConverter.convertCompanyToDto(c);
				//System.out.println("Compañia modificada correctamente en la Base de Datos");
				return companyDtoMod;
				
			}
			else {
				logger.error("No existe ninguna company con el nombre: " + nombreCompany);
				throw new CompanyBadRequestException("No existe ninguna company con el nombre: " + nombreCompany); //OK
			}
		}


}
