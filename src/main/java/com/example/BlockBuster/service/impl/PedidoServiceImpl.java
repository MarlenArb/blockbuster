package com.example.BlockBuster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.dto.TiendaDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.entities.Tienda;
import com.example.BlockBuster.exceptions.pedidoExceptions.PedidoBadRequestException;
import com.example.BlockBuster.exceptions.pedidoExceptions.PedidoForbiddenException;
import com.example.BlockBuster.exceptions.pedidoExceptions.PedidoImUsedException;
import com.example.BlockBuster.exceptions.pedidoExceptions.PedidoNotFoundException;
import com.example.BlockBuster.exceptions.tiendaExceptions.TiendaBadRequestException;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.PedidoRepository;
import com.example.BlockBuster.repositories.TiendaRepository;
import com.example.BlockBuster.service.PedidoService;
import com.example.BlockBuster.service.Converters.PedidoConverter;



@Service
public class PedidoServiceImpl implements PedidoService {
	
	Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired	
	private PedidoRepository  pedidoRepository;
	
	@Autowired
	private TiendaRepository  tiendaRepository;
	
	@Autowired
	private PedidoConverter  pedidoConverter;
	
	private List<Pedido> pedidos = new ArrayList<>();
	private List<PedidoDto> pedidosDto = new ArrayList<>();
	

	//Add new order to data base --OK
	@Override
	public PedidoDto addPedido(PedidoDto pedidoDto) throws Exception {

		Cliente c = clienteRepository.findByDocumento(pedidoDto.getDocumentoCliente());
		if( c == null) {
			
			logger.error("No existe ningún cliente con el documento: " + pedidoDto.getDocumentoCliente());
			throw new PedidoNotFoundException("No existe ningún cliente con el documento: " + pedidoDto.getDocumentoCliente());
		}
		
		Tienda t = tiendaRepository.findByNombreTienda(pedidoDto.getNombreTienda());
		if(t == null) {
			
			logger.error("No existe ninguna tienda con el nombre: " + pedidoDto.getNombreTienda());
			throw new PedidoNotFoundException("No existe ninguna tienda con el nombre: " + pedidoDto.getNombreTienda());
			
		}
		
		Pedido newPedido = new Pedido();
		newPedido = pedidoRepository.findByRefPedido(pedidoDto.getRefPedido());
		if (newPedido == null) {
			
			newPedido = pedidoConverter.convertPedidoToEntity(pedidoDto);
			
			//1 pedido N juegos
			c.getPedidos().add(newPedido); //N pedidos 1 cliente
			t.getPedidos().add(newPedido); //N pedidos 1 tienda
			
			
			pedidoRepository.save(newPedido);
			
			PedidoDto pedidoDtoMod = new PedidoDto();
			pedidoDtoMod = pedidoConverter.convertPedidoToDto(newPedido);
			return pedidoDtoMod;
			
		}
		else {
			logger.error("Ya existe un pedido con la referencia: " + pedidoDto.getRefPedido());
			throw new PedidoForbiddenException("Ya existe un pedido con la referencia: " + pedidoDto.getRefPedido());
		}
	}
	
	
	//Look for an order from a data base --OK 
	@Override
	public PedidoDto getPedido(String refPedido) throws Exception {
		Pedido p = new Pedido ();
		p = pedidoRepository.findByRefPedido(refPedido);
		if ( p != null) {
			PedidoDto pdto = pedidoConverter.convertPedidoToDto(p);
			return pdto;
		}
		else {
			logger.error("No existe ningun pedido con la referencia: " + refPedido);
			throw new PedidoNotFoundException("No existe ningun pedido con la referencia: " + refPedido);
		}
	}
	


	//Remove an exists order  --OK
	@Override 
	public void removePedido(String refPedido) throws Exception { 
		Pedido p = new Pedido();
		p = pedidoRepository.findByRefPedido(refPedido);
		if( p != null) {
		pedidoRepository.delete(p);
			System.out.println("Pedido borrado de la base de datos");
		}
		else {
			logger.error("No existe ningun pedido con la referencia: " + refPedido);
			throw new PedidoNotFoundException("No existe ningun pedido con la referencia: " + refPedido);
		}
	}
	

	//Show me all the orders --BUCLE INFINITO SI LO SACO POR EL POSTMAN
	@Override
	public List<PedidoDto> getLista() throws Exception { 
		pedidosDto.clear();
		pedidos = pedidoRepository.findAll();
		if( pedidos.size() != 0) {
			PedidoDto pedidoDto = new PedidoDto();
			for(Pedido pedido: pedidos) {
				pedidoDto = pedidoConverter.convertPedidoToDto(pedido);
				pedidosDto.add(pedidoDto);
				}
			
				return pedidosDto;
			}
		else
			throw new PedidoBadRequestException("No hay ningún pedido registrado en la base de datos"); 
			//TODO: funciona pero deberia ser nocontent, lo que ese no me muestra mensaje ninguno  
		
	}
	
	
	//Modify an order from a data base --OK
	@Override
	public PedidoDto modify(String refPedido, PedidoDto pedidoMod) throws Exception {
		Pedido p = new Pedido();
		p = pedidoRepository.findByRefPedido(refPedido);
		
		if( p != null) {
			
			if(pedidoMod.getRefPedido()!= null) {
				p.setRefPedido(pedidoMod.getRefPedido());;
			}
			
			if(pedidoMod.getEstado()!= null) {
				p.setEstado(pedidoMod.getEstado());;
			}
			
			if(pedidoMod.getDocumentoCliente() != null) {
				Cliente c = clienteRepository.findByDocumento(pedidoMod.getDocumentoCliente());
				if( c == null) {
					
					throw new PedidoNotFoundException("No existe ningún cliente con el documento especificado"); // OK
				}
				else p.setCliente(clienteRepository.findByDocumento(pedidoMod.getDocumentoCliente()));
			}
			
			if(pedidoMod.getNombreTienda() != null) {
				Tienda t = tiendaRepository.findByNombreTienda(pedidoMod.getNombreTienda());
				if(t == null) {
					
					throw new PedidoNotFoundException("No existe ninguna tienda con el nombre especificado");  // OK
					
				}
				else p.setTienda(tiendaRepository.findByNombreTienda(pedidoMod.getNombreTienda()));
			}
			
			pedidoRepository.save(p);
			PedidoDto pedidoDtoMod = new PedidoDto();
			pedidoDtoMod = pedidoConverter.convertPedidoToDto(p);
			return pedidoDtoMod;
		}
		
		else {
			logger.error("No existe ningun pedido con la referencia: " + refPedido);
			throw new PedidoForbiddenException("No existe ningun pedido con la referencia: " + refPedido);  // OK
		}
			
	}

}

