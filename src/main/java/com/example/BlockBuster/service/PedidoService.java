package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.entities.Pedido;

@Service
public interface PedidoService {
	
	public PedidoDto getPedido(String refPedido) throws Exception;
	public PedidoDto addPedido(PedidoDto pedido) throws Exception;
	public void removePedido(String refPedido) throws Exception;
	public List<PedidoDto>getLista() throws Exception;
	public PedidoDto modify(String refPedido, PedidoDto pedidoMod) throws Exception;

}
