package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.entities.Cliente;

@Service
public interface ClienteService {
	
	public ClienteDTO getCliente(String documento) throws Exception;
	public ClienteDTO addCliente(ClienteDTO cliente) throws Exception;
	public void removeCliente(String documento) throws Exception;
	public List<ClienteDTO>getLista() throws Exception;
	public ClienteDTO modify(String nombreCliente, ClienteDTO clienteMod) throws Exception;
	public void agregarJson();

}
