package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.dto.CompanyDto;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;

@Service
public interface CompanyService {
	
	public CompanyDto getCompany(String nombreCompany);
	public CompanyDto addCompany(CompanyDto company) ;
	public void removeCompany(String nombreCompany) ;
	public List<CompanyDto>getLista();
	public CompanyDto modify(String nombreCompany, CompanyDto companyMod);

}
