package com.example.BlockBuster.service.Converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.CompanyDto;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;
import com.example.BlockBuster.repositories.JuegoRepository;


@Service
public class CompanyConverter {
	
	private List<String> juegosRef = new ArrayList<>();
	private List<Juego> juegos = new ArrayList<>();
	
	@Autowired
	private JuegoRepository juegoRepository;
	
		//Convert entity to object
		public CompanyDto convertCompanyToDto(Company entCompany) { 
			
			 CompanyDto dtoCompany = new CompanyDto();
			 dtoCompany.setCif(entCompany.getCif());
			 dtoCompany.setNombreCompany(entCompany.getNombreCompany());
			 
			 juegosRef.clear();
				for (Juego juego : entCompany.getJuegos()) {

					juegosRef.add(juego.getRefJuego());
				}

				dtoCompany.setJuegos(juegosRef);
			
				
			 return dtoCompany;
			
		}
		
		
		//Convert object to entity
		public Company convertCompanyToEntity(CompanyDto dtoCompany) {
			
			Company entCompany = new Company();
			entCompany.setCif(dtoCompany.getCif());
			entCompany.setNombreCompany(dtoCompany.getNombreCompany());;
			
			juegos.clear();
			for(String refJuego: dtoCompany.getJuegos()) {
				juegos.add(juegoRepository.findByRefJuego(refJuego));
			}
			entCompany.setJuegos(juegos);

			 //TODO: Me los coge perfecto asi, pero me da error en angular, solo debo pasarle a mi back juego.refJuego y no Juego completo!!
			return entCompany;
			
		}

}
