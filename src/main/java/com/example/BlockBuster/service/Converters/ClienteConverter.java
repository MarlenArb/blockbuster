package com.example.BlockBuster.service.Converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.ClienteDTO;
import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;
import com.example.BlockBuster.exceptions.generic.BadRequestException;
import com.example.BlockBuster.exceptions.rolesExceptions.RolBadRequestException;
import com.example.BlockBuster.repositories.RolRepository;

@Service
public class ClienteConverter {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private RolRepository rolRepository;
	
	
	private List<EnumRol> roles = new ArrayList<>();
	//Convert entity to object
	public ClienteDTO convertClienteToDto(Cliente entCliente) { 
		
		 ClienteDTO dtoCliente = new ClienteDTO();
		 dtoCliente.setNombreCliente(entCliente.getNombreCliente());
		 dtoCliente.setPassword(entCliente.getPassword());
		 dtoCliente.setDocumento(entCliente.getDocumento());
		 dtoCliente.setCorreo(entCliente.getCorreo());
		 dtoCliente.setFechaNac(entCliente.getFechaNac());
		 roles.clear();
			for (Rol rol : entCliente.getRoles()) {

				roles.add(EnumRol.valueOf( rol.getRol())); //TODO: No me añade a la lista el rol ADMIN solo USER solo al listar con 
															//get cliente si lo hace bien
			}

			dtoCliente.setRoles(roles);

		 return dtoCliente;
		
	}
	
	
	//Convert object to entity
	public Cliente convertClienteToEntity(ClienteDTO dtoCliente) {
		
		Cliente entCliente = new Cliente();
		entCliente.setNombreCliente(dtoCliente.getNombreCliente());;
		entCliente.setUsername(dtoCliente.getNombreCliente());//TODO: por ahora mi username es mi nombre
		entCliente.setPassword(encoder.encode(dtoCliente.getPassword()));
		entCliente.setFechaNac(dtoCliente.getFechaNac());
		entCliente.setDocumento(dtoCliente.getDocumento());
		entCliente.setCorreo(dtoCliente.getCorreo());
		if (dtoCliente.getRoles().isEmpty()) {
			Rol newRol = rolRepository.findByRol("USER");
			if (newRol == null) {
				throw new RolBadRequestException("El rol USER no se encuentra en la lista de roles");
			
			}
			entCliente.getRoles().add(newRol);
			newRol.getClientes().add(entCliente);
			}
		 else {
			
			for(EnumRol rol : dtoCliente.getRoles()) {
				Rol newRol = rolRepository.findByRol(rol.toString());
				if (newRol == null) {
					throw new RolBadRequestException("El rol especificado no se encuentra en la lista de roles");
				}
				entCliente.getRoles().add(newRol);
				newRol.getClientes().add(entCliente);
			}
		}
		return entCliente;
		
	}
}
	

		

