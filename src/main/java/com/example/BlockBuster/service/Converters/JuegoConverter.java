package com.example.BlockBuster.service.Converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.JuegoDto;
import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.entities.Company;
import com.example.BlockBuster.entities.Juego;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.repositories.CompanyRepository;
import com.example.BlockBuster.repositories.PedidoRepository;

@Service
public class JuegoConverter {

	@Autowired
	PedidoRepository pedidoRepository;
	@Autowired
	CompanyRepository companyRepository;

	private List<String> nombreCompanies = new ArrayList<>();
	
	// Convert entity to object
	public JuegoDto convertJuegoToDto(Juego entJuego) {

		JuegoDto dtoJuego = new JuegoDto();
		dtoJuego.setTitulo(entJuego.getTitulo());
		dtoJuego.setRefJuego(entJuego.getRefJuego());
		dtoJuego.setFechaLanzamiento(entJuego.getFechaLanzamiento());
		dtoJuego.setCategoria(entJuego.getCategoria());
		dtoJuego.setPegi(entJuego.getPegi());
		dtoJuego.setRefPedido(entJuego.getPedido().getRefPedido());

		nombreCompanies.clear();
		for (Company company : entJuego.getCompanies()) {

			nombreCompanies.add(company.getNombreCompany());
		}

		dtoJuego.setCompanies(nombreCompanies);

		return dtoJuego;

	}

	public Juego convertJuegoToEntity(JuegoDto dtoJuego) {

		Juego entJuego = new Juego();
		entJuego.setTitulo(dtoJuego.getTitulo());
		entJuego.setRefJuego(dtoJuego.getRefJuego());
		entJuego.setFechaLanzamiento(dtoJuego.getFechaLanzamiento());
		entJuego.setCategoria(dtoJuego.getCategoria());
		entJuego.setPegi(dtoJuego.getPegi());
		entJuego.setPedido(pedidoRepository.findByRefPedido(dtoJuego.getRefPedido()));

//		
//		p.getJuegos().add(newJuego); // N juegos 1 pedido
//		
//		companies = dtoJuego.getCompanies();
//		for(Company company : companies) {
//			Company c = companyRepository.findByNombreCompany(company.getNombreCompany());
//			c.getJuegos().add(dtoJuego); // N juegos M companie
//			dtoJuego.getCompanies().add(c);
//			//companyRepository.save(c);
//			
//			
//		}

		return entJuego;

	}

}
