package com.example.BlockBuster.service.Converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.PedidoDto;
import com.example.BlockBuster.entities.Pedido;
import com.example.BlockBuster.repositories.ClienteRepository;
import com.example.BlockBuster.repositories.TiendaRepository;

@Service
public class PedidoConverter {
	
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private TiendaRepository tiendaRepository;
	
	//Convert entity to object
	public PedidoDto convertPedidoToDto(Pedido entPedido) { 
		
		 PedidoDto dtoPedido = new PedidoDto();
		 dtoPedido.setEstado(entPedido.getEstado());
		 dtoPedido.setRefPedido(entPedido.getRefPedido());
		 dtoPedido.setDocumentoCliente(entPedido.getCliente().getDocumento());
		 dtoPedido.setNombreTienda(entPedido.getTienda().getNombreTienda());
		 
		 return dtoPedido;
		
	}
	
	
	//Convert object to entity
	public Pedido convertPedidoToEntity(PedidoDto dtoPedido) {
		
		Pedido entPedido = new Pedido();
		entPedido.setEstado(dtoPedido.getEstado());
		entPedido.setRefPedido(dtoPedido.getRefPedido());
		entPedido.setCliente(clienteRepository.findByDocumento(dtoPedido.getDocumentoCliente()));
		entPedido.setTienda(tiendaRepository.findByNombreTienda(dtoPedido.getNombreTienda()));
		

		return entPedido;
		
	}
	

}
