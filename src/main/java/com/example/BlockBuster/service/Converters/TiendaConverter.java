package com.example.BlockBuster.service.Converters;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.TiendaDto;
import com.example.BlockBuster.entities.Tienda;

@Service
public class TiendaConverter {
	
	//Convert entity to object
		public TiendaDto convertTiendaToDto(Tienda entTienda) { 
			
			 TiendaDto dtoTienda = new TiendaDto();
			 dtoTienda.setNombreTienda(entTienda.getNombreTienda());
			 dtoTienda.setDireccion(entTienda.getDireccion());
			 
			 return dtoTienda;
			
		}
		
		
		//Convert object to entity
		public Tienda convertTiendaToEntity(TiendaDto dtoTienda) {
			
			Tienda entTienda = new Tienda();
			entTienda.setNombreTienda(dtoTienda.getNombreTienda());
			entTienda.setDireccion(dtoTienda.getDireccion());


			return entTienda;
			
		}

}
