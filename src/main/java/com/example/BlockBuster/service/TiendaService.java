package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.dto.TiendaDto;
import com.example.BlockBuster.entities.Tienda;

@Service
public interface TiendaService {
	
	public TiendaDto getTienda(String nombreTienda) throws Exception;
	public TiendaDto addTienda(TiendaDto tienda) throws Exception;
	public void removeTienda(String nombreTienda)throws Exception ;
	public List<TiendaDto>getLista() throws Exception;
	public TiendaDto modify(String nombreTienda, TiendaDto tienaMod) throws Exception;

}
