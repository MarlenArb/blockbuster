package com.example.BlockBuster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.enums.EnumRol;

@Service 
public interface RolService {

	//public String addRol(String rol, String documentoCliente) throws Exception;
	public void initRol(Boolean añadirRoles);
	public String addAdmin(String documentoCliente) throws Exception;
}
