package com.example.BlockBuster.exceptions.tiendaExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class TiendaBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Tienda Bad Request";
	
	public TiendaBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}