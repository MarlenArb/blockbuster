package com.example.BlockBuster.exceptions.tiendaExceptions;

import com.example.BlockBuster.exceptions.generic.NoContentException;

public class TiendaNoContentException extends NoContentException {
	
	private static final String DESCRIPTION = "Tienda No Content";
	
	public TiendaNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}

