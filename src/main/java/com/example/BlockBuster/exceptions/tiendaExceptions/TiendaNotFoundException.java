package com.example.BlockBuster.exceptions.tiendaExceptions;

import com.example.BlockBuster.exceptions.generic.NotFoundException;

public class TiendaNotFoundException extends NotFoundException {
	
	private static final String DESCRIPTION = "Tienda Not Found";
	
	public TiendaNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}