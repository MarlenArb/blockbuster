package com.example.BlockBuster.exceptions.tiendaExceptions;

import com.example.BlockBuster.exceptions.generic.ForbiddenException;

public class TiendaForbiddenException extends ForbiddenException {
	
	private static final String DESCRIPTION = "Tienda Forbidden";
	
	public TiendaForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
