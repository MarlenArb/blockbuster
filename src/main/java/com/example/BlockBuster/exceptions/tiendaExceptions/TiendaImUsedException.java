package com.example.BlockBuster.exceptions.tiendaExceptions;

import com.example.BlockBuster.exceptions.generic.ImUsedException;

public class TiendaImUsedException extends ImUsedException{
	
	private static final String DESCRIPTION = "Tienda Im Used";
	
	public TiendaImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
