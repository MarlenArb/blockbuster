package com.example.BlockBuster.exceptions.companyExceptions;

import com.example.BlockBuster.exceptions.generic.NotFoundException;

public class CompanyNotFoundException extends NotFoundException{
	
	private static final String DESCRIPTION = "Company Not Found";
	
	public CompanyNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}