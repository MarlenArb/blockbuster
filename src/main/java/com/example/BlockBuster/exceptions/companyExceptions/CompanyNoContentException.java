package com.example.BlockBuster.exceptions.companyExceptions;

import com.example.BlockBuster.exceptions.generic.NoContentException;

public class CompanyNoContentException extends NoContentException {
	
	private static final String DESCRIPTION = "Company No Content";
	
	public CompanyNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}

