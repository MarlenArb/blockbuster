package com.example.BlockBuster.exceptions.companyExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class CompanyBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Company Bad Request";
	
	public CompanyBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}

