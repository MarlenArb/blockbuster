package com.example.BlockBuster.exceptions.companyExceptions;

import com.example.BlockBuster.exceptions.generic.ImUsedException;

public class CompanyImUsedException extends ImUsedException{
	
	private static final String DESCRIPTION = "Company Im Used";
	
	public CompanyImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}