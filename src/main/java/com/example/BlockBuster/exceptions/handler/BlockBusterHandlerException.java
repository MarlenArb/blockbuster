package com.example.BlockBuster.exceptions.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.BlockBuster.dto.ErrorResponse;
import com.example.BlockBuster.exceptions.generic.BadRequestException;
import com.example.BlockBuster.exceptions.generic.ForbiddenException;
import com.example.BlockBuster.exceptions.generic.ImUsedException;
import com.example.BlockBuster.exceptions.generic.NoContentException;
import com.example.BlockBuster.exceptions.generic.NotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class BlockBusterHandlerException extends ResponseEntityExceptionHandler {
	
	
	//NOT_FOUND
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class})
	@ResponseBody
	public ErrorResponse notFoundException(HttpServletRequest request, Exception exception) {
	
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	
	//NO_CONTENT
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({NoContentException.class})
	@ResponseBody
	public ErrorResponse noContentException(HttpServletRequest request, Exception exception) {
	
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	
	//BAD_REQUEST
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({
		org.springframework.dao.DuplicateKeyException.class,
		javax.validation.ConstraintViolationException.class,
		//java.sql.SQLIntegrityConstraintViolationException.class, //prueba
		BadRequestException.class
	})
	@ResponseBody
	public ErrorResponse badRequestException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	
	//IM_USED
	@ResponseStatus(HttpStatus.IM_USED)
	@ExceptionHandler({ImUsedException.class})
	@ResponseBody
	public ErrorResponse imUsedException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}

	//FORBIDDEN
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({ForbiddenException.class})
	@ResponseBody
	public ErrorResponse forbiddenException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream().map( e -> e.getDefaultMessage()).collect(Collectors.toList());
		return new ResponseEntity<Object>(new ErrorResponse(ex, errorMessages, ((ServletWebRequest)request).getRequest().getRequestURI()), status); 
	}
}
