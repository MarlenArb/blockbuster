package com.example.BlockBuster.exceptions.juegoExceptions;

import com.example.BlockBuster.exceptions.generic.ImUsedException;

public class JuegoImUsedException extends ImUsedException{
	
	private static final String DESCRIPTION = "Juego Im Used";
	
	public JuegoImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
