package com.example.BlockBuster.exceptions.juegoExceptions;

import com.example.BlockBuster.exceptions.generic.NotFoundException;

public class JuegoNotFoundException extends NotFoundException {
	
	private static final String DESCRIPTION = "Juego Not Found";
	
	public JuegoNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
