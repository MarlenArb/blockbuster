package com.example.BlockBuster.exceptions.juegoExceptions;

import com.example.BlockBuster.exceptions.generic.NoContentException;

public class JuegoNoContentException extends NoContentException{
	
	private static final String DESCRIPTION = "Juego No Content";
	
	public JuegoNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}
