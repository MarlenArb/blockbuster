package com.example.BlockBuster.exceptions.juegoExceptions;

import com.example.BlockBuster.exceptions.generic.ForbiddenException;

public class JuegoForbiddenException extends ForbiddenException {
	
	private static final String DESCRIPTION = "Juego Forbidden";
	
	public JuegoForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
