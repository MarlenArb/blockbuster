package com.example.BlockBuster.exceptions.juegoExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class JuegoBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Juego Bad Request";
	
	public JuegoBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}
