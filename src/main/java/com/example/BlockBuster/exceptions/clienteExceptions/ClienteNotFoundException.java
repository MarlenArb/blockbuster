package com.example.BlockBuster.exceptions.clienteExceptions;

import com.example.BlockBuster.exceptions.generic.NotFoundException;

public class ClienteNotFoundException extends NotFoundException {
	
	private static final String DESCRIPTION = "Cliente Not Found";
	
	public ClienteNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
