package com.example.BlockBuster.exceptions.clienteExceptions;

import com.example.BlockBuster.exceptions.generic.ForbiddenException;

public class ClienteForbiddenException extends ForbiddenException {
	
	private static final String DESCRIPTION = "Cliente Forbidden";
	
	public ClienteForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
