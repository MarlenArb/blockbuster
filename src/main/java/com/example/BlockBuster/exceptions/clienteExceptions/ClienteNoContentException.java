package com.example.BlockBuster.exceptions.clienteExceptions;

import com.example.BlockBuster.exceptions.generic.NoContentException;

public class ClienteNoContentException extends NoContentException {
	
	private static final String DESCRIPTION = "Cliente No Content";
	
	public ClienteNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
