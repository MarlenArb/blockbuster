package com.example.BlockBuster.exceptions.clienteExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class ClienteBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Juego Bad Request";
	
	public ClienteBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}
