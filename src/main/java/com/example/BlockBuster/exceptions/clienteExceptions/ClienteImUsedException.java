package com.example.BlockBuster.exceptions.clienteExceptions;

import com.example.BlockBuster.exceptions.generic.ImUsedException;

public class ClienteImUsedException extends ImUsedException{
	
	private static final String DESCRIPTION = "Cliente Im Used";
	
	public ClienteImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
