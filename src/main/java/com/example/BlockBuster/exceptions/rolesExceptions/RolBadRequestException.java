package com.example.BlockBuster.exceptions.rolesExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class RolBadRequestException extends BadRequestException{

	private static final String DESCRIPTION = "Rol Bad Request";
	
	public RolBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}
