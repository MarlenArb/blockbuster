package com.example.BlockBuster.exceptions.pedidoExceptions;

import com.example.BlockBuster.exceptions.generic.ImUsedException;

public class PedidoImUsedException extends ImUsedException{
	
	private static final String DESCRIPTION = "Pedido Im Used";
	
	public PedidoImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
