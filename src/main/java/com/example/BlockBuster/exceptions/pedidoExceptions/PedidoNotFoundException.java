package com.example.BlockBuster.exceptions.pedidoExceptions;

import com.example.BlockBuster.exceptions.generic.NotFoundException;

public class PedidoNotFoundException extends NotFoundException {
	
	private static final String DESCRIPTION = "Pedido Not Found";
	
	public PedidoNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
