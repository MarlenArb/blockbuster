package com.example.BlockBuster.exceptions.pedidoExceptions;

import com.example.BlockBuster.exceptions.generic.BadRequestException;

public class PedidoBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Pedido Bad Request";
	
	public PedidoBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}
}
