package com.example.BlockBuster.exceptions.pedidoExceptions;

import com.example.BlockBuster.exceptions.generic.ForbiddenException;

public class PedidoForbiddenException extends ForbiddenException {
	
	private static final String DESCRIPTION = "Pedido Forbidden";
	
	public PedidoForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
		
	}

}
