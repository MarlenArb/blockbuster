package com.example.BlockBuster.exceptions.generic;

public class ImUsedException extends RuntimeException {
	
	private static final String DESCRIPTION = "Bad Request Exception (226)";
	
	public ImUsedException(String details) {
		super(DESCRIPTION + ". " + details);
	}

}
