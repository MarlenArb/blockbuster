package com.example.BlockBuster.exceptions.generic;

public class NoContentException extends RuntimeException {
	
	private static final String DESCRIPTION = "No Content Exception (204)";
	
	public NoContentException(String details) {
		super(DESCRIPTION + ". " + details);
	}


}
