package com.example.BlockBuster.userConfigurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.BlockBuster.entities.Cliente;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private UserServiceImpl userServiceDetails;
	
	//@Autowired 
	//private BCryptPasswordEncoder bCrypt;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
	//	BCryptPasswordEncoder bCryptPasswordEncoder = ;
		return new BCryptPasswordEncoder();
	}
	
	
	
	@Autowired //en el video override
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception{
      auth.userDetailsService(userServiceDetails).passwordEncoder(passwordEncoder());
      
	}
	
//	@Autowired
//    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception{
//        auth
//        	.inMemoryAuthentication()
//        		.withUser("admin")
//        		.password(passwordEncoder().encode("123"))
//        		.roles("USER", "ADMIN");
//        //PRUEBA
//        auth
//    	.inMemoryAuthentication()
//    		.withUser("user")
//    		.password(passwordEncoder().encode("123"))
//    		.roles("USER");
//    }


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		
		http.csrf().disable().httpBasic().and().authorizeRequests().anyRequest().permitAll(); 
//		http.authorizeRequests()
//		//ROLES
//		.antMatchers("/addAdmin").hasAuthority("ADMIN")
//		.antMatchers("/initRol").hasAuthority("ADMIN")
//		//CLIENTES
//		.antMatchers("/cliente").permitAll() //TODO: Probando
//		//COMPANY
//		//.antMatchers("/listaCompany").hasAuthority("ADMIN")
//		.antMatchers("/company").hasAuthority("ADMIN")
//		.antMatchers("/modCompany").hasAuthority("ADMIN")
//		//TIENDAS
//		.antMatchers(HttpMethod.POST,"/tienda").hasAuthority("ADMIN")
//		.antMatchers(HttpMethod.DELETE,"/tienda").hasAuthority("ADMIN")
//		.antMatchers("/modTienda").hasAuthority("ADMIN")
//		//PEDIDOS
//		.antMatchers(HttpMethod.GET, "/pedido").hasAuthority("ADMIN")
//		.antMatchers(HttpMethod.DELETE,"/pedido").hasAuthority("ADMIN")
//		.antMatchers("/modPedido").hasAuthority("ADMIN")
//		//.antMatchers("/listaPedido").hasAuthority("ADMIN")
//		//JUEGOS
//		.antMatchers("/juego").hasAuthority("ADMIN")
//		.and().csrf().disable().httpBasic();
		
		//http.authorizeRequests().anyRequest().authenticated().and().csrf().disable().httpBasic();
		//.and().cors().configurationSource(corsConfigurationSource());
	
	}
	

}

