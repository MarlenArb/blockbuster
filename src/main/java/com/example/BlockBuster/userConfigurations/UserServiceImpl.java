package com.example.BlockBuster.userConfigurations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.BlockBuster.entities.Cliente;
import com.example.BlockBuster.entities.Rol;
import com.example.BlockBuster.repositories.ClienteRepository;


@Service
@Transactional
public class UserServiceImpl implements UserDetailsService{

	@Autowired
	private ClienteRepository clienteRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException { 
		
		Cliente c = clienteRepository.findByUsername(username);//TODO: aqui
		
		List<GrantedAuthority> roles = new ArrayList<>();
		for (Rol rol: c.getRoles()) {
			roles.add(new SimpleGrantedAuthority(rol.getRol().toString()));
		}
		UserDetails userDet = new User(c.getNombreCliente(), c.getPassword(), roles);
			
			
		return userDet;
	}

}
